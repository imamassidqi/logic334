﻿//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();
//Tugas7();
Tugas8();
//Tugas9();
//Tugas10();
Console.ReadKey();

static void Tugas1()
{
    Console.WriteLine("Cara Duduk");
    Console.Write("Masukkan jumlah anak (x) : ");
    int x = int.Parse(Console.ReadLine());
    //int[] faktorial = new int[x];
    int jumlah = 1;
    // int index = 0;
    string detail = ""; 
    for (int i = x; i >= 1; i--)
    {
        //faktorial[index] = i;
        jumlah = jumlah * i;
        detail += detail == "" ? i : " + " + i;
      //  index++;
    }
    //Console.WriteLine($"{x}!  = {string.Join(" x ", faktorial)} = {jumlah}");
    Console.WriteLine($"{x}!  = {detail} = {jumlah}");
    Console.WriteLine($"Ada {jumlah} cara");

}
 
 static void Tugas2()
{
    Console.WriteLine("Program Menghitung Sinyal Kode Morse");
    Console.Write("Masukkan Kode Morse : ");
    string kodemorse = Console.ReadLine().ToUpper();
    int sinyalsalah = 0;
    int jumlahsinyalbenar = 0;
    string sinyalbenar = "SOS";
    string sinyalyangbenar = "";
    if ((kodemorse.Length % 3) != 0)
    {
        Console.WriteLine("Input Tidak Valid");
    }
    else
    {
        for (int i = 0; i < kodemorse.Length; i+=3)
        {
            if ("SOS" == kodemorse.Substring(i, 3))
            {
                jumlahsinyalbenar++;
            }
            else
            {
                sinyalsalah++;
            }
            sinyalyangbenar += "SOS";
        }
        Console.WriteLine($"Total Sinyal Benar : {jumlahsinyalbenar}");
        Console.WriteLine($"Total Sinyal Salah : {sinyalsalah}");
        Console.WriteLine($"Sinyal yang benar adalah :{sinyalyangbenar}");
    }
}

static void Tugas3()
{
    Console.WriteLine($"Program Peminjaman Buku");
    Console.Write("Masukkan tanggal peminjaman (format: dd-MM-yyyy):");
    string date1 = Console.ReadLine();

    Console.Write("Masukkan tanggal pengembalian (format: dd-MM-yyyy):");
    string date2 = Console.ReadLine();

    Console.Write("Masukkan jumlah hari wajib kembali:");
    int dateexp = int.Parse(Console.ReadLine());

    try
    {
        DateTime datetime1 = DateTime.ParseExact(date1, "d-MM-yyyy", null);
        DateTime datetime2 = DateTime.ParseExact(date2, "d-MM-yyyy", null);
        Console.WriteLine(date1);
        Console.WriteLine(date2);

        TimeSpan interval = datetime2 - datetime1;
        Console.WriteLine($"total hari :{interval.Days} hari");

        int pinalti = interval.Days - dateexp;
        Console.WriteLine($"Total keterlambatan pengembalian adalah = {pinalti} hari");
        int denda = pinalti * 500;
        Console.WriteLine($"Denda yang harus dibayarkan adalah = {denda} Rupiah");
    }
    catch (Exception ex)
    {
        Console.WriteLine("format yang anda masukkan salah");
        Console.WriteLine("Pesan error : " + ex.Message);
    }

}

static void Tugas4()
{
    Console.WriteLine("Soal No 4");
    Console.Write("Masukkan tanggal mulai (dd/mm/yyy) : ");
    string input = Console.ReadLine().ToUpper();
    string[] splitInput = input.Split('/');
    int[] splitInputInt = Array.ConvertAll(splitInput, int.Parse);
    try
    {
        DateTime dt = DateTime.ParseExact(input, "dd/MM/yyyy", null);
        DateTime dt1 = new DateTime(splitInputInt[2], splitInputInt[1], splitInputInt[0]);
        Console.Write("Masukkan Hari akan Dilaksanakannya Ft1 : ");
        int hariFt1 = int.Parse(Console.ReadLine());
        Console.Write("Masukkan tgl Hari Libur : ");
        string[] hariLibur = Console.ReadLine().Split(",");
        int[] hariLiburInt = Array.ConvertAll(hariLibur, int.Parse);
        //int totalHariLibur = hariLiburInt.Length;

        for (int i = 0; i < hariFt1; i++)
        {
            if ((int)dt1.DayOfWeek == 0 || (int)dt1.DayOfWeek == 6)
            {
                hariFt1 += 1;
            }
            else
            {
                for (int j = 0; j < hariLiburInt.Length; j++)
                {
                    if (hariLiburInt[j] == dt1.Day)
                    {
                        hariFt1 += 1;
                    }
                }
            }
            dt1 = dt1.AddDays(1);
        }
        Console.WriteLine(dt1.ToString("dd/MM/yyyy"));
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
    }

}

static void Tugas5()
{
    Console.WriteLine("Program Menghitung Jumlah Huruf Vokal dan Huruf Konsonan");
    Console.Write("Masukkan Kata : ");
    string huruf = Console.ReadLine().ToLower();
    int jumlahVokal = 0;
    int  jumlahKonsonan = 0;
   
    for (int i = 0; i < huruf.Length; i++) 
    {
        
        char c = huruf[i];
        if (Char.IsLetter(c)) 
        {
            if ("aiueo".Contains(c)) 
            {
               jumlahVokal++ ;
            }
            else
            {
                jumlahKonsonan++ ;
            }
        }
    }
    Console.WriteLine($"Jumlah Huruf Vokal adalah : {jumlahVokal}");
    Console.WriteLine($"Jumlah Huruf Konsonan adalah : {jumlahKonsonan}");
}

static void Tugas6() 
{
    Console.WriteLine("Program Mencetak Setiap Huruf dengan Bintang");
    Console.Write("Masukkan Kata : ");
    string kata = Console.ReadLine();

    foreach (char huruf in kata)
    {
        if (Char.IsLetter(huruf))
        {
            Console.WriteLine(new string('*', 3) + huruf + new string('*', 3));
        }
    }
}

static void Tugas7()
{
    Console.WriteLine("Program Menghitung Pembayaran dan Sisa Uang Elsa");
    Console.Write("Total Menu: ");
    int totalMenu = int.Parse(Console.ReadLine());

    Console.Write("Index Makanan Alergi: ");
    int indexMakananAlergi = int.Parse(Console.ReadLine());

    Console.Write("Harga Menu (pisahkan dengan koma): ");
    string[] hargaMenu = Console.ReadLine().Split(',');
    int[] hargaMenuInt = Array.ConvertAll(hargaMenu, int.Parse);
    Console.Write("Uang Elsa: ");
    int uangElsa = int.Parse(Console.ReadLine());

    // Hitung total makanan yang dimakan Elsa dan Dimas
    int totalMakanan = 0;
    for (int i = 0; i < hargaMenuInt.Length; i++)
    {
        if (i == indexMakananAlergi)
        {
            continue;
        }
        totalMakanan += hargaMenuInt[i];
    }

    // Hitung berapa yang harus dibayar oleh Elsa
    int totalBayar = totalMakanan/2;
    int sisaUangElsa = uangElsa - totalBayar;

    Console.WriteLine($"Elsa harus membayar = {totalBayar}");
    if (sisaUangElsa > 0)
    {
        Console.WriteLine($"Sisa uang Elsa = {sisaUangElsa}");
    }
    else if (sisaUangElsa < 0)
    {
        Console.WriteLine($"Uang Elsa Kurang = {sisaUangElsa}");
    }
    else
    {
        Console.WriteLine("Uang Pas");
    }
}
static void Tugas8()
{
    Console.WriteLine("Program Mencetak Pola");
    // Meminta pengguna memasukkan angka
    Console.Write("Masukkan angka: ");
    int angka = int.Parse(Console.ReadLine());
   
    for (int i = 0; i <= angka; i++)
    {
        for (int j = i; j < angka; j++)
        {
            Console.Write(" ");
        }
        for (int k = 0; k <= i; k++)
        {
            Console.Write("#");
        }
        Console.WriteLine();
    }
}

static void Tugas9()
{
    Console.WriteLine("Program Difference");
    Console.Write("Masukkan ukuran matrix  : ");
    int panjang = int.Parse(Console.ReadLine());
    int[,] matrix = new int[panjang, panjang];
    int dia1 = 0;
    int dia2 = 0;

    for (int i = 0; i < panjang; i++)
    {
        for (int j = 0; j < panjang; j++)
        {
            Console.Write($"input {i} {j} : ");
            matrix[i, j] = int.Parse(Console.ReadLine());

            if (i == j) // Diagonal utama
            {
                dia1 += matrix[i, j];
            }

            if (i + j == panjang - 1) // Diagonal kedua
            {
                dia2 += matrix[i, j];
            }
        }
    }
    Console.WriteLine("Jumlah elemen diagonal utama (dia1) : " + dia1);
    Console.WriteLine("Jumlah elemen diagonal kedua (dia2) : " + dia2);
    Console.WriteLine("Selisih (dia1 - dia2) : " + (dia1 - dia2));
}

static void Tugas10()
{
    Console.WriteLine("Mencari angka tertinggi dan jumlah angka tertinggi");
    Console.Write("Masukkan Input angka : ");
    string[] input = Console.ReadLine().Split(" ");
    int[] inputInt = Array.ConvertAll(input, int.Parse);
    int nilaiMax = 0;
    int count = 0;
    for (int i = 0; i < inputInt.Length; i++)
    {
        nilaiMax = Math.Max(nilaiMax, inputInt[i]);
    }
    for (int i = 0; i < input.Length; i++)
    {
        if (nilaiMax == inputInt[i])
        {
            count++;
        }
    }
    Console.WriteLine("Nilai tertinggi berjumlah : " + count);
}