﻿//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();
//Tugas7();
//Tugas8();
Console.ReadKey();
static void Tugas1 ()
{
    Console.WriteLine("--Program Perhitungan Gaji Karyawan--");
    Console.Write("Masukkan Golongan : ");
    int golongan = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Jam Kerja : ");
    int jamkerja = int.Parse(Console.ReadLine());

    int upahperjam = 0;
    double upah = 0;
    double lembur = 0;
    if (golongan == 1)
    { upahperjam = 2000; }
    else if (golongan == 2)
    { upahperjam = 3000; }
    else if (golongan == 3) 
    { upahperjam = 4000; }
    else if (golongan == 4)
    { upahperjam = 5000; }
    else
    { Console.WriteLine("golongan tidak valid"); }

    if (jamkerja <= 40)
    {
        upah = upahperjam * jamkerja;
        lembur = 0;
    }
    else
    {
        upah = upahperjam * 40;
        lembur = (jamkerja - 40) * (1.5 * upahperjam);
    }
    Console.WriteLine($"Upah : Rp.{upah}");
    Console.WriteLine($"Lembur : Rp.{lembur}");
    Console.WriteLine($"Total Gaji : Rp.{upah + lembur}");
}

static void Tugas2()
{
    Console.WriteLine("--Program Pemisah Kata--");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan String : ");
    string split = Console.ReadLine();
    string[] katakata = kalimat.Split(split);
   int nomor = 0;
    for (int i = 0; i < katakata.Length; i++)
    {
       if (katakata[i]=="")
       {
            continue;
        }
        nomor++;
        Console.WriteLine($"kata {nomor} = {katakata[i]}");
    }
    Console.WriteLine($"Total kata adalah = {nomor} ");
}

static void Tugas3()
{
    Console.WriteLine("Program Mengganti Kata dengan *");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    for (int i = 0; i < katakata.Length; i++)
    {
        for (int j = 0; j < katakata[i].Length; j++)
        {
            if (j == 0 || j == katakata[i].Length - 1)
            {
                Console.Write(katakata[i][j]);
            }
            else
            {
                Console.Write("*");
            }
        }
        Console.Write(" ");
    }
}
static void Tugas4()
{
    Console.WriteLine("Program Mengganti Kata dengan *");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    for (int i = 0;i < katakata.Length;i++) 
    {
        for (int j = 0; j < katakata[i].Length; j++)
        {
            if (j == 0 || j== katakata[i].Length -1)
            {
                Console.Write("*");
            }
            else
            {
                Console.Write(katakata[i][j]);
            }
        }
        Console.Write(" ");
    }
}
static void Tugas5()
{
    Console.WriteLine("--Program Menghapus Huruf--");
    Console.Write("Masukkan Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] katakata = kalimat.Split(" ");
    for (int i = 0; i < katakata.Length; i++)
    {
        for (int j = 0; j < katakata[i].Length; j++)
        {
            if (j == 0)
            {
            }
            else
            {
                Console.Write(katakata[i][j]);
            }
        }
        Console.Write(" ");
    }
}

static void Tugas6()
{
    Console.Write("Masukkan Jumlah N = ");
    int jumlah = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Angkanya : ");
    int nilai = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kelipatannya : ");
    int kelipatan = int.Parse(Console.ReadLine());
    for (int i = 0; i < jumlah; i++)
    {
        if (i % 2 == 0)
        {
            Console.Write(nilai);
        }
        else
        {
            Console.Write(" * ");
        }
        nilai = nilai * kelipatan;
    }
}

static void Tugas7()
{
    Console.Write("Masukkan Nilai N : ");
    int input = int.Parse(Console.ReadLine());
    int[] array = new int[input];
    array[0] = 1;
    array[1] = 1;

    for (int i = 0; i < array.Length; i++)
    {
        if (i >= 2)
        {
            array[i] = array[i - 1] + array[i - 2];
        }
        Console.Write(array[i]+ ",");
        
    }
}

static void Tugas8()
{
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    for (int i = 0; i <= nilai; i++)
    {
        for (int j = 0; j <= nilai; j++)
        {
            if (i == 0)
            {
                Console.Write($"{j} ");
            }
            else if (i == 10)
            {
                Console.Write($"{i + 1 - j} ");
            }
            else if (j == 0 || j == nilai )
            {
                Console.Write("* ");
            }
            else
            {
                Console.Write("  ");
            }
        }
        Console.WriteLine();
    }
}

