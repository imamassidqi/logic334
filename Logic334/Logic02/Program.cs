﻿//IfStatement();
//IfElseStatement();
//IfElseIfStatement();
//IfNested();
//Ternary();
Switch();
Console.ReadKey();

static void Switch()
{
    Console.WriteLine("--Switch--");
    Console.Write("Pilih buah kesukaan Anda (apel, mangga, melon) :");
    string pilihan = Console.ReadLine().ToLower();

    switch (pilihan)
    {
        case "apel":
            Console.WriteLine("Anda memilih buah apel");
            break;
        case "mangga":
            Console.WriteLine("Anda memilih buah mangga");
            break;
        case "melon":
            Console.WriteLine("Anda memilih buah melon");
            break;
        default:
            Console.WriteLine("Anda memilih yang lain");
            break;
    }
}
static void Ternary()
{
    Console.WriteLine("--Ternary--");
    Console.Write("Masukkan Nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai y : ");
    int y = int.Parse(Console.ReadLine());
    if (x > y)
    {
        Console.WriteLine("x lebih besar dari pada y");
    }
    else
    {
        Console.WriteLine("x lebih kecil atau sama dengan y");
    }
    string z = x > y ? "x lebih besar dari pada y" : "x lebih kecil atau sama dengan y"; //ternary
    Console.WriteLine(z);
}
static void IfNested()
{
    Console.WriteLine("--If Nested--");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    if(nilai >= 50)
    {
        Console.WriteLine("Kamu Berhasil");

        if(nilai == 100)
        {
            Console.WriteLine("Kamu Keren");
        }
    }
    else
    {
        Console.WriteLine("Kamu Gagal");
    }
}
static void IfElseIfStatement()
{

    Console.WriteLine("--If Else If Statement--");
    Console.Write("Masukkan Nilai x : ");
    int x = int.Parse(Console.ReadLine());

    if(x == 10)
    {
        Console.WriteLine("x value equals to 10");
    }
    else if(x > 10)
    {
        Console.WriteLine("x value greater than 10");
    }
    else
    {
        Console.WriteLine("x value less than 10");
    }
}
static void IfElseStatement()
{
    Console.WriteLine("--If Else Statement--");
    Console.Write("Masukkan Nilai x : ");
    int x = int.Parse(Console.ReadLine());
   
    if (x >= 10)
    {
        Console.WriteLine("x is greater than or equal 10");
    }
    else
    {
        Console.WriteLine("y is lesser than 10");
    }
}
static void IfStatement ()
{
    Console.WriteLine("--IfStatement--");
    Console.Write("Masukkan Nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai y : ");
    int y = int.Parse(Console.ReadLine());

    if (x >= 10)
    {
        Console.WriteLine("x is greater than or equal 10");
    }

    if (y <= 5)
    {
        Console.WriteLine("y is lesser than or equal 5");
    }
}