﻿//Padleft();
//Rekursif();
Console.ReadKey();

static void Rekursif()
{
    Console.WriteLine("--Rekursif Function--");
    Console.Write("Masukkan Input Awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Input Akhir : ");
    int end = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Type (ASC/DESC) : ");
    string type = Console.ReadLine().ToUpper();
    //panggil fungsi
    perulangan(start, end, type); 
}

static int perulangan(int start, int end, string type)
{
    if (type == "ASC")
    {
        if (start == end)
        {
            Console.WriteLine(end);
            return 0;
        }

        Console.WriteLine(start);
        return perulangan(start + 1, end, type);
    }
    else
    {
        if (start == end)
        {
            Console.WriteLine(end);
            return 0;
        }
        Console.WriteLine(end);
        return perulangan(start, end - 1, type);
    }
}
/*static int perulangan(int start, int end)
{
    if (start == end)
    {
        Console.WriteLine(end);
        return 0;
    }
    Console.WriteLine(start);
    return perulangan(start +1, end);
}*/

static void Padleft()
{
    Console.WriteLine("--Pad Left--");
    Console.Write("Masukkan Input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Panjang Karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Char : ");
    char chars = char.Parse(Console.ReadLine());
    //231100001 -> 2 digit tahun + 2 digit bulan + generate length

    DateTime date = DateTime.Now;
    string code = string.Empty; //atau ;

    code = date.ToString("yyMM") + input.ToString().PadLeft(panjang, chars);
    Console.WriteLine($"Hasil Padleft : {code}");
}