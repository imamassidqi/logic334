﻿
//konversi();
//OperatorAritmatika();
//Modulus();
//OperatorPenugasan();
//OperatorPerbandingan();
//OperatorLogika();
MethodReturnType();
Console.ReadKey();

static void MethodReturnType()
{
    Console.WriteLine("--Cetak Method Return Type--");
    Console.Write("Masukkan mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);

    Console.WriteLine($"Hasil mangga + apel = {jumlah}");
}
static int hasil(int mangga, int apel)
{
    int hasil = 0;

    hasil = mangga + apel;

    return hasil;
}
static void OperatorLogika()
{
    Console.WriteLine("--Operator Logika--");
    Console.Write("Enter Your age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Password : ");
    string password = Console.ReadLine();

    bool isAdult = age > 18;
    bool isPasswordValid = password == "admin";

    if(isAdult && isPasswordValid)
    {
        Console.WriteLine("Welcome to the Jungle");
    }
    else
    {
        Console.WriteLine("Sorry, Try Again");
    }
}
static void OperatorPerbandingan()
{
    int mangga, apel  = 0;

    Console.WriteLine("--Operator Perbandingan--");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");

}
static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 20;

    //isi ulang variabel
    mangga = 5;

    Console.WriteLine("--Operator Penugasan--");
    Console.Write("Masukkan mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine($"mangga : {mangga}");
    Console.Write("Masukkan apel : ");
    apel = int.Parse(Console.ReadLine());

    //operator penugasan
    apel += 6;

    Console.WriteLine($"apel += 6 : {apel}");

}
static void OperatorAritmatika()
{
    int Mangga, Apel, Hasil = 0;

    Console.WriteLine("--Operator Aritmatika--");
    Console.Write("Masukkan Mangga : ");
    Mangga = Convert.ToInt32(Console.ReadLine());
    //Mangga = int.Parse(Console.Readline()); opsi 2
    Console.Write("Masukkan Apel : ");
    Apel = Convert.ToInt32(Console.ReadLine());

    Hasil = Mangga + Apel;
    Console.WriteLine($"Hasil Penjumlahan = {Hasil}");
}

//Modulus
static void Modulus()
{
    int Mangga, Orang, Hasil = 0;

    Console.WriteLine("--Operator Modulus--");
    Console.Write("Masukkan Mangga : ");
    Mangga = Convert.ToInt32(Console.ReadLine());
    //Mangga = int.Parse(Console.Readline()); opsi 2
    Console.Write("Masukkan Orang : ");
    Orang = Convert.ToInt32(Console.ReadLine());

    Hasil = Mangga % Orang;
    Console.WriteLine($"Hasil Modulus = {Hasil}");
}
    //Fungsi/method
    static void konversi()
{
    Console.WriteLine("--Konversi--");
  
    int myInt = Convert.ToInt32(Console.ReadLine());
    double myDouble = Convert.ToDouble(Console.ReadLine());
    bool myBool = false;

    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();
    double doubleMyInt = Convert.ToDouble(myInt);
    int intMyDouble = Convert.ToInt32(myDouble);

    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);
    Console.WriteLine(doubleMyInt);
    Console.WriteLine(intMyDouble);
    Console.WriteLine(myBool.ToString());
}