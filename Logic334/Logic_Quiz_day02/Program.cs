﻿

//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
Tugas6();
Console.ReadKey();
static void Tugas4()
{
    int PuntungRokok, HargaperBatang, jumlahBatang, penghasilan = 0;
    Console.WriteLine("--Program Perhitungan Rokok--");
    Console.Write("Masukkan Jumlah Puntung Rokok : ");
    PuntungRokok = Convert.ToInt32(Console.ReadLine());
    Console.Write("Harga Per Batang : ");
    HargaperBatang = Convert.ToInt32(Console.ReadLine());
    jumlahBatang = PuntungRokok / 8 ;
    Console.WriteLine($"Hasil batang Rokok yang dirangkai = {jumlahBatang}");
    penghasilan = jumlahBatang * HargaperBatang;
    Console.WriteLine($"Penghasilan dari penjualan rokok = Rp {penghasilan}");
}
static void Tugas6()
{
    Console.WriteLine("--Program Pengecekan Angka Ganjil atau Genap--");
    Console.Write("Masukkan Angka : ");
    int x = int.Parse(Console.ReadLine());
    if (x % 2 == 0)
    {
        Console.WriteLine($"Angka {x} adalah Genap");
    }
    else
    {
        Console.WriteLine($"Angka {x} adalah Ganjil");
    }
}
static void Tugas5()
{
    Console.WriteLine("--Program Penentuan Grade Berdasarkan Nilai--");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());


    if (nilai >= 80 && nilai <= 100)
    {
        Console.WriteLine("Kamu mendapatkan Grade A");
    }
    else if (nilai >= 60 && nilai <80)
    {
        Console.WriteLine("Kamu mendapatkan Grade B");
    }
    else if (nilai < 60 && nilai >= 0)
    {
        Console.WriteLine("Kamu mendapatkan Grade C");
    }
    else
    {
        Console.WriteLine("Nilai Tidak Valid");
    }
}
static void Tugas3()
{
    Console.WriteLine("--Menghitung Modulus--");
    Console.Write("Masukkan Angka : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Pembagi : ");
    int pembagi = int.Parse(Console.ReadLine());

    
    if (angka % pembagi == 0) 

    {
        Console.WriteLine($"{angka} % {pembagi} adalah 0 ");
    }
    else
    {
        Console.WriteLine($"{angka} % {pembagi} bukan 0 melainkan : {angka % pembagi}" );
    }
}
static void Tugas2()
{
    int sisi, keliling, luas = 0;

    Console.WriteLine("--Menghitung Keliling dan Luas Persegi--");
    Console.Write("Masukkan Sisi Persegi : ");
    sisi = int.Parse(Console.ReadLine());

    //menghitung keliling
    keliling = 4 * sisi;

    //menghitung luas
    luas = sisi * sisi;

    Console.WriteLine("Hasil Keliling Pesergi adalah : " + keliling + " cm ");
    Console.WriteLine("Hasil Luas Persegi adalah : " + luas + " cm ");
}
static void Tugas1()
{
    double jari2, keliling, luas = 0;
    Console.WriteLine("--Menghitung Keliling dan Luas Lingkaran--");
    Console.Write("Masukkan jari-jari lingkaran : ");
    jari2 = Convert.ToDouble(Console.ReadLine());

    //menghitung keliling
    keliling = 2 * Math.PI * jari2;
    int mykeliling = Convert.ToInt32(keliling);
    //menghitung luas
    luas = Math.PI * jari2 * jari2;
    int myluas = Convert.ToInt32(luas);

    Console.WriteLine("Hasil Keliling Lingkaran adalah : " + mykeliling + " cm ");
    Console.WriteLine("Hasil Luas Lingkaran adalah : " + myluas + " cm ");
}
