﻿

//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();
Tugas7();
//Tugas8();
Console.ReadKey();
static void Tugas1()
{
    Console.WriteLine("--Program Deret Angka--");
    Console.Write("Masukkan Jumlah N = ");
    int jumlah = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai awal = ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kelipatan = ");
    int kelipatan = int.Parse(Console.ReadLine());

    for (int i = 0; i < jumlah; i++)
    {
        if(i % 2 != 0)
        {
        }
        else
        {
            Console.Write("-");
        }
        Console.Write($"{angka}\t");
        angka += kelipatan;
    }
}

static void Tugas2()
{
    Console.WriteLine("Time Conversion");
    Console.Write("Masukkan Waktu (hh:mm:ssAM/PM): ");
    string waktu = Console.ReadLine().ToUpper();
    try
    {
        DateTime date1 = DateTime.ParseExact(waktu, "hh:mm:sstt", null);
        Console.WriteLine(date1.ToString("HH:mm:ss"));
    }
    catch (Exception e)
    {
        Console.WriteLine("Format yang anda masukkan salah!!");
        Console.WriteLine("Pesan error : " + e.Message);
    }
}

static void Tugas3()
{
    Console.WriteLine("Program Menghitung Harga Pakaian");
    Console.Write("Masukkan Kode Baju (1/2/3): ");
    int kode = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Ukuran Baju : ");
    string ukuran = (Console.ReadLine().ToUpper());

    int harga = 0;
    string merk = "";
    switch (kode)
    {
        case 1:
            merk = "IMP";
            break;
        case 2:
            merk = "Prada";
            break;
        case 3:
            merk = "Gucci";
            break;
        default:
            Console.WriteLine("Kode baju tidak valid");
            return;
    }
    if (kode == 1) 
    {
        if (ukuran == "S")
        {
            harga = 200000;
        }
        else if (ukuran == "M")
        {
            harga = 220000;
        }
        else
        {
            harga = 250000;
        }

    }
    else if (kode == 2)
        {
        if (ukuran == "S")
        {
            harga = 150000;
        }
        else if (ukuran == "M")
        {
            harga = 160000;
        }
        else
        {
            harga = 170000;
        }
    }
    else if (kode == 3)
    {
        if (ukuran == "S" || ukuran == "M")
        {
            harga = 200000;
        }
        else
        {
            harga = 200000;
        }
    }
    else
    {
        Console.WriteLine("Tidak ada Harga yang dapat ditampilkan");
    }
    Console.WriteLine($"Merk Baju = {merk}");
    Console.WriteLine($"Harga = {harga.ToString("#,#.00")}");
}

static void Tugas4()
{
    Console.WriteLine("Andi Belanja Lebaran");
    Console.Write("Masukkan Uang Andi : ");
    int uang = int.Parse(Console.ReadLine());
    int[] hargabaju = { 35, 40, 50, 20 };
    //int[] hargabaju = Array.ConvertAll<
    int[] hargacelana = { 40, 30, 45, 10 };

    int totalbelanja = 0;

    for (int i = 0; i < hargabaju.Length && i < hargacelana.Length; i++)
    {
        int totalharga = hargabaju[i] + hargacelana[i];

        if (totalharga <= uang && totalharga > totalbelanja)
        {
            totalbelanja = totalharga;
            //totalbelanja = Math.Max(totalbelanja, totalharga);
        }
    }
    Console.WriteLine($"Total belanja Andi yang bisa dibeli: {totalbelanja}");
}

static void Tugas5()
{
    Console.WriteLine("Program Memindahkan Angka");
    Console.Write("Masukkan split  : ");
    string split = Console.ReadLine();
    Console.Write("Masukkan angka  : ");
    string[] input = Console.ReadLine().Split(split);
    Console.Write("Masukkan rot: ");
    int rot = int.Parse(Console.ReadLine());

    for (int i = 1; i <= rot; i++)
    {
        Console.Write($"{i} : ");
        for (int j = i; j < input.Length; j++)
        {
            Console.Write(input[j] + ",");
        }
        for (int a = 0; a < i; a++)
        {
            Console.Write(input[a]);
            if (a != i - 1)
            {
                Console.Write(",");
            }
        }
        Console.WriteLine();
    }
}

static void Tugas6()
{
    Console.WriteLine("Program Bubble Sort");

    Console.Write("Masukkan angka-angka : ");
    string input = Console.ReadLine();
    string[] angkaString = input.Split(',');

    int[] angka = new int[angkaString.Length];

    for (int i = 0; i < angkaString.Length; i++)
    {
        angka[i] = int.Parse(angkaString[i]);
    }

    for (int i = 0; i < angka.Length - 1; i++)
    {
        for (int j = 0; j < angka.Length - 1 - i; j++)
        {
            if (angka[j] > angka[j + 1])
            {
                // Menukar angka jika angka sebelumnya lebih besar
                int temp = angka[j];
                angka[j] = angka[j + 1];
                angka[j + 1] = temp;
            }
        }
    }
    Console.Write("Hasil pengurutan : ");
    for (int i = 0; i < angka.Length; i++)
    {
        Console.Write(angka[i]);
        if (i < angka.Length - 1)
        {
            Console.Write(",");
        }
    }
    Console.WriteLine();
}

static void Tugas7()
{
    Console.WriteLine("--Array--");
    Console.Write("Masukkan banyaknya data (n) : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 0; i < n; i++)
    {
        if (i >= 0)
        {
            Console.Write($"{angka} ");
            angka += tambah;
        }
    }
}

static void Tugas8()
{
    Console.WriteLine("--Array Berbintang--");
    Console.Write("Masukkan banyaknya data (n) : ");
    int n = int.Parse(Console.ReadLine());
    Console.Write("Masukkan angka awal : ");
    int angka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan penambah : ");
    int tambah = int.Parse(Console.ReadLine());

    for (int i = 1; i <= n; i++)
    {
        if (i % 3 == 0)
        {
            Console.Write("* ");
        }
        else
        {
            Console.Write($"{angka} ");
            angka += tambah;
        }
    }
}
