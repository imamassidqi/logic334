﻿// See https://aka.ms/new-console-template for more information
//Output
Console.WriteLine("Hello, World!");
Console.Write("Hello Gaes ");
Console.WriteLine("Welcome Back");
Console.Write("Masukkan Nama : ");

//Inpt
string nama = Console.ReadLine();
string kelas = "Batch 334";
int umur = 19;
bool isExpired = false;

//variabel mutable
kelas = "Batch 334 .Net";

//variabel immutable
const double phi = 3.14;

Console.WriteLine(nama);

//Penggabungan string
Console.WriteLine("Hi, {0} Selamat Datang! {1} {2}", nama, kelas, umur);
Console.WriteLine("Hi, " + nama + " Selamat Datang! " + kelas + "");
Console.WriteLine($"Hi, {nama} Selamat Datang!");

Console.ReadKey(); 