﻿
using Logic06;
//AbstractClass();
//ObjectClass();
//Constructor();
//Encapsulation();
//Inheritance();
Overriding();
Console.ReadKey();

static void Overriding()
{
    Console.WriteLine("--Overriding--");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    Console.WriteLine($"Kucing {kucing.pindah()}");
    Console.WriteLine($"Paus {paus.pindah()}");
}
static void Inheritance()
{
    Console.WriteLine("Inheritance");
    TypeMobil typeMobil = new TypeMobil();
    typeMobil.Civic();
}
static void Encapsulation()
{
    Console.WriteLine("Encapsulation");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;

    pp.TampilkanData();
}

static void Constructor()
{
    Console.WriteLine("Constructor");
    Mobil mobil = new Mobil("B 123 MX");
    string platno = mobil.getPlatno();
    //string platno = mobil.platno;
    Console.WriteLine($"Mobil dengan Nomor Polisi : {platno}");
}
static void ObjectClass()
{
    Console.WriteLine("--Object Class--");
    Mobil mobil = new Mobil("") { nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0};
    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);
    mobil.utama();
}

static void AbstractClass()
{
    Console.WriteLine("--Abstract Class--");
    Console.Write("Masukkan Input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Input y : ");
    int y = int.Parse(Console.ReadLine());

    TestTurunan calc = new TestTurunan();
    int jumlah = calc.jumlah(x, y);
    int kurang = calc.kurang(x, y);

    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}