﻿
Console.ReadKey();

    static int solveMeFirst(int a, int b)
{
    // Hint: Type return a+b; below  
    return a + b;
}

static void Main(String[] args)
{
    int val1 = Convert.ToInt32(Console.ReadLine());
    int val2 = Convert.ToInt32(Console.ReadLine());
    int sum = solveMeFirst(val1, val2);
    Console.WriteLine(sum);
}

class Result
{
    /*
     * Complete the 'simpleArraySum' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts INTEGER_ARRAY ar as parameter.
     */
    public static int simpleArraySum(List<int> ar)
    {
        int sum = 0;
        foreach (int i in ar)
        {
            sum += i;
        }
        return sum;
    }
}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int arCount = Convert.ToInt32(Console.ReadLine().Trim());

        List<int> ar = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(arTemp => Convert.ToInt32(arTemp)).ToList();

        int result = Result.simpleArraySum(ar);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}

class Result1
{
    /*
     * Complete the 'staircase' function below.
     *
     * The function accepts INTEGER n as parameter.
     */
    public static void staircase(int n)
    {
        for (int i = n; i > 0; i--)
        {
            for (int j = 1; j <= n; j++)
            {
                if (i <= j)
                {
                    Console.Write("#");
                }
                else
                {
                    Console.Write(" ");
                }
            }
            Console.WriteLine();
        }
        return;
    }

}
class Solution1
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine().Trim());

        Result1.staircase(n);
    }
}
