﻿//Tugas1();
//Tugas2();
//Tugas3();
//Tugas4();
//Tugas5();
//Tugas6();
//Tugas7();
Tugas8();
Console.ReadKey();

static void Tugas1()
{
    Console.WriteLine("Program Penentuan Grade Nilai");
    Console.Write("Masukkan Nilai Anda : ");
    int nilai = int.Parse(Console.ReadLine());

    if (nilai >= 90 && nilai <=100)
    {
        Console.WriteLine("Kamu Mendapatkan Grade A");
    }
    else if (nilai >= 70 && nilai <=89)
    {
        Console.WriteLine("Kamu Mendapatkan Grade B");
    }
    else if (nilai >= 50 && nilai <=69)
    {
        Console.WriteLine("Kamu Mendapatkan Grade C");
    }
    else if (nilai <50 && nilai >=0)
    {
        Console.WriteLine("Kamu Mendapatkan Grade E");
    }
    else
    { Console.WriteLine("Nilai Tidak Valid"); }
}

static void Tugas2()
{
    Console.WriteLine("Program Poin Pulsa");
    Console.Write("Masukkan Nominal Pembelian : ");
        int beli = int.Parse(Console.ReadLine());
    if (beli>=10000 && beli <25000)
    {
        Console.WriteLine("Kamu mendapatkan point 80");
    }
    else if (beli>=25000 && beli<50000)
    {
        Console.WriteLine("Kamu mendapatkan point 200");
    }
    else if (beli>=50000 && beli<100000)
    {
        Console.WriteLine("Kamu mendapatkan point 400");
    }
    else if (beli == 100000)
    {
        Console.WriteLine("Kamu mendapatkan point 800");
    }
    else
    {
        Console.WriteLine("Nominal Tidak Valid");
    }
}

static void Tugas3()
{
    Console.WriteLine("--Program Menghitung Voucher Diskon Grab--");
    Console.Write("Masukkan Nominal Pembelanjaan : Rp. ");
    int belanja=int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kode Promo : ");
    string kodepromo = Console.ReadLine();
    if (belanja<30000)
    {
        Console.WriteLine("Tidak ada diskon");
    }

    else
    {
        if (kodepromo == "JKTOVO")
        {
            //menghitung diskon maksimum
            int diskon = belanja * 40 / 100;
            Console.Write("Masukkan Jarak : ");
            int jarak = int.Parse(Console.ReadLine());

            int ongkir = 5000;
            if (jarak > 5)
            {
                int extraongkir = jarak - 5;
                ongkir += extraongkir * 1000;

            }
            int totalbelanja = belanja - diskon + ongkir;
            Console.WriteLine($"Belanja : Rp. {belanja}");
            Console.WriteLine($"Diskon : Rp. {diskon}");
            Console.WriteLine($"Ongkir : Rp. {ongkir}");
            Console.WriteLine($"Total Biaya Pesanan adalah sebesar Rp. {totalbelanja} ");
            
        }
    }
}


static void Tugas5()
{
    Console.WriteLine("Program Istilah Generasi Kelahiran");
    Console.Write("Masukkan Nama Anda : ");
    string nama = Console.ReadLine();
    Console.Write("Tahun Berapa Anda Lahir : ");
    int tahunlahir = int.Parse(Console.ReadLine());
    string generasi = hitunggenerasi(tahunlahir);
    Console.WriteLine($"Nama : {nama}");
    Console.WriteLine($"Generasi Kamu adalah : {generasi}");
   
}
static string hitunggenerasi(int tahunlahir)
{
    if(tahunlahir >= 1944 && tahunlahir <=1964)
    {
        return "Baby boomer";
    }
    else if (tahunlahir >= 1965 && tahunlahir <= 1979)
    {
        return "Generasi X";
    }
    if (tahunlahir >= 1980 && tahunlahir <= 1994)
    {
        return "Generasi Y";
    }
    if (tahunlahir >= 1995 && tahunlahir <= 2015)
    {
        return "Generasi Z";
    }
    else
    {
        return "Generasi Tidak diketahui";
    }
}

static void Tugas7 ()
{
    Console.WriteLine("Program Penentuan BMI (Body Mass Index)");
    Console.Write("Masukkan Berat Badan Kamu : ");
    double berat = Convert.ToDouble(Console.ReadLine());
    Console.Write("Masukkan Tinggi Badan Kamu : ");
    double tinggicm = Convert.ToDouble(Console.ReadLine());
    double tinggim = tinggicm / 100;
    double BMI = berat / (tinggim * tinggim);
    
    string kategori = "";
    if (BMI < 18.5 )
    {
        kategori = "kurus";
    }
    else if (BMI >=18.5 && BMI <25)
    {
        kategori = "langsing/sehat";
    }
    else
    {
        kategori = "gemuk";
    }
    Console.WriteLine($"Nilai BMI anda adalah : {Math.Round(BMI,2)}");
    Console.WriteLine($"Anda termasuk : {kategori}");
}

static void Tugas8()
{
    int ratarata = 0;
    Console.WriteLine("Program Penentuan Nilai");
    Console.Write("Masukkan Nilai MTK : ");
    int nilaimtk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Fisika : ");
    int nilaifisika = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Nilai Kimia : ");
    int nilaikimia = int.Parse(Console.ReadLine());

    ratarata = (nilaimtk + nilaifisika + nilaikimia) / 3;
    Console.WriteLine($"Nilai rata-rata kamu adalah {ratarata}");

    if (ratarata >= 50) 
    {
        Console.WriteLine("Kamu Berhasil, Kamu Hebat");
    }
    else
    {
        Console.WriteLine("Maaf, Kamu GAGAL");
    }
    
}

static void Tugas4()
{
    Console.WriteLine("Program Belanja dengan promo di Shopee");
    
    Console.Write("Masukkan Nominal Belanja : Rp.");
    int belanja = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Ongkos Kirim : Rp.");
    int ongkir = int.Parse(Console.ReadLine());

    Console.Write("Pilih Voucher (1, 2, atau 3) : ");
    int voucher = int.Parse(Console.ReadLine());

    int diskonongkir = 0;
    int diskonbelanja = 0;

    if (voucher == 1)
    {
        if (belanja >= 30000)
        {
            diskonongkir = 5000;
            diskonbelanja = 5000;
        }
    }
    else if (voucher == 2) 
    {
        if (belanja >= 50000)
        {
            diskonongkir = 10000;
            diskonbelanja = 10000;
        }
    }
    else if (voucher == 3)
    {
        if (belanja >= 100000)
        {
            diskonongkir = 20000;
            diskonbelanja = 10000;
        }
    }

    int totalbelanja = belanja - diskonbelanja;
    int totalongkir = ongkir - diskonongkir;
    if (diskonongkir > ongkir)
    {
        diskonongkir = ongkir;
    }
    int totalharga = totalbelanja + totalongkir;
    Console.WriteLine("");
    Console.WriteLine($"Nominal Belanja: Rp.{belanja}");
    Console.WriteLine($"Ongkos Kirim Rp.{ongkir}");
    Console.WriteLine($"Diskon Ongkir : Rp.{diskonongkir}");
    Console.WriteLine($"Diskon Belanja : Rp.{diskonbelanja}");
    Console.WriteLine($"Total Harga : Rp.{totalharga}");

}

static void Tugas6()
{
    Console.WriteLine("Program Menghitung Slip Gaji Karyawan");
    Console.Write("Masukkan Nama Karyawan : ");
    string nama = Console.ReadLine();
    Console.Write("Masukkan Tunjangan : ");
    int tunjangan = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Gaji Pokok : ");
    int gajipokok = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Banyak Bulan : ");
    int bulan = int.Parse(Console.ReadLine());

    double pajak = hitungpajak(gajipokok, tunjangan);
    double bpjs = 0.03 * (gajipokok + tunjangan);
    double gajiperbulan = (gajipokok + tunjangan) - (pajak + bpjs);
    double totalgaji = gajiperbulan * bulan;

    Console.WriteLine();
    Console.WriteLine($"Karyawan atas nama {nama} memiliki slip gaji sebagai berikut : ");
    Console.WriteLine($"Pajak : Rp.{pajak}");
    Console.WriteLine($"BPJS : Rp.{bpjs}");
    Console.WriteLine($"Gaji/bln : Rp.{gajiperbulan}");
    Console.WriteLine($"Total gaji/banyak bulan : Rp.{totalgaji}");
}
    static double hitungpajak(int gajipokok, int tunjangan)
{
    double totalpendapatan = gajipokok + tunjangan;
    double pajak = 0.0;

    if (totalpendapatan <= 5000000)
    {
        pajak = 0.05 * totalpendapatan;
    }
    else if (totalpendapatan > 5000000 && totalpendapatan <= 10000000)
    {
        pajak = 0.10 * totalpendapatan;
    }
    else
    {
        pajak = 0.15 * totalpendapatan;
    }

    return pajak;
}