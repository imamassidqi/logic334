--SQLQuery1

--DDL (Data Definition Language)
/*CREATE*/
--create database
create database db_kampus

use db_kampus

--drop database
drop database db_kampus

--create table
create table mahasiswa(
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null
)

--create view
create view vw_mahasiswa
as select * from mahasiswa

/*ALTER*/